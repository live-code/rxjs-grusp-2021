import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './features/user/user.component';
import { UsersComponent } from './features/users/users.component';
import { DemoForkjoinComponent } from './features/demo-forkjoin/demo-forkjoin.component';
import { DemoSwitchmapComponent } from './features/demo-switchmap/demo-switchmap.component';
import { DemoSwitchmap2Component } from './features/demo-switchmap2/demo-switchmap2.component';
import { DemoPollingComponent } from './features/demo-polling/demo-polling.component';
import { DemoMeteoExampleComponent } from './features/demo-meteo-example/demo-meteo-example.component';
import { AdminGuard } from './core/auth/admin-guard.service';

const routes: Routes = [
  { path: 'demo-meteo', component: DemoMeteoExampleComponent },
  { path: 'demo-polling', component: DemoPollingComponent },
  { path: 'demo-switchmap/:id', component: DemoSwitchmapComponent},
  { path: 'demo-switchmap2', component: DemoSwitchmap2Component},
  { path: 'demo-forkjoin', component: DemoForkjoinComponent},
  { path: 'user', component: UserComponent},
  { path: 'user/:id', component: UserComponent},
  { path: 'users', component: UsersComponent},
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'admin', canActivate: [AdminGuard], loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
  { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
