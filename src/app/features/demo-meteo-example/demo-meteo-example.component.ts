import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { of, OperatorFunction, pipe } from 'rxjs';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-demo-meteo-example',
  template: `
    <input type="text" [formControl]="input">
    <pre *ngIf="meteo">
      {{ meteo | json}}
    </pre>
  `,
})
export class DemoMeteoExampleComponent {
  input: FormControl = new FormControl();
  meteo: any;

  constructor(http: HttpClient) {
    this.input.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        map((text: string) => text.toUpperCase()),
        switchMap(
          text => http.get(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(() => of(null))
            )
        ),
        // filter(meteo => !!meteo)
      )
      .subscribe(res => {
        this.meteo = res;
      });

    // default value
    // this.form.get('city').setValue('milano')
    // this.form.patchValue({ city: 'milano'})
    this.input.setValue('Milan');


    // CUSTOOM OPERATOR USAGE
    this.input.valueChanges
      .pipe(
        searchMeteo(
          'http://api.openweathermap.org/data/2.5/weather?units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534&q=',
          2000, 4
        ),
      )
      .subscribe(res => {
        console.log(res)
      });
  }
}

/**
 * Custom Operator
 * @param url
 * @param debounce
 * @param minLength
 */
const searchMeteo = (
  url: string,
  debounce: number = 1000,
  minLength: number = 0,
): OperatorFunction<string, any> => {
  return pipe(
    filter(text => text.length > minLength),
    debounceTime(debounce),
    // map((text: string) => text.toUpperCase()),
    switchMap(
      text => ajax.get(`${url}${text}`)
        .pipe(
          catchError(() => of(null))
        )
    ),
  );
};



