import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'app-login',
  template: `
   
    <form [formGroup]="form" (ngSubmit)="authService.login(form.value)">
      <input type="text" formControlName="username">
      <input type="text" formControlName="password">
      <button>Sign In</button>
    </form>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder, public authService: AuthService) {
    this.form = fb.group({
      username: 'pippo',
      password: 'pluto',
    });
  }

  ngOnInit(): void {
  }

}
