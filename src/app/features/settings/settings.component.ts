import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    
    <h1>Settings</h1>
    
    <button (click)="themeService.setTheme('dark')">dark</button>
    <button (click)="themeService.setTheme('light')">light</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) { }

  ngOnInit(): void {
  }

}
