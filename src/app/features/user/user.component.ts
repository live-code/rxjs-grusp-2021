import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { catchError, filter, map, shareReplay, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

interface SimpleUser {
  id: number;
  name: string;
}

@Component({
  selector: 'app-user',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form #f="ngForm" (ngSubmit)="save(f)">
      <input type="hidden" [ngModel]="(user$ | async)?.id" name="id">
      <input type="text" [ngModel]="(user$ | async)?.name" name="username">
      <button>
        {{(user$ | async) ? 'EDIT' : 'ADD' }}
      </button>
    </form>
    
    <hr>
    
    <ng-container>
      <pre>PARAM ID: {{ (user$ | async)?.id}}</pre>
      <h2>{{(users$ | async)?.length }} users</h2>
      <li *ngFor="let u of users$ | async" 
          [routerLink]="'/user/' + u.id"
          [style.color]="u.id === (user$ | async)?.id ? 'red' : null"
      >{{u.name}} - {{u.id}} -  {{(user$ | async)?.id}}</li>
    </ng-container>
    
    {{update()}}
  `,
})
export class UserComponent {
  users$: Observable<User[]> = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      catchError(err => of([]))
    );

  user$: Observable<SimpleUser> = this.activatedRoute.params
    .pipe(
      filter(({ id }) => !!id),
      switchMap(
        ({id}) => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
          .pipe(catchError(err => of(null)))
      ),
      filter(res => !!res),
      map(({ id, name }) => ({ id, name })),
      shareReplay(1)
    );

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute) {
  }

  save(form: NgForm): void {
    if (form.value.id) {
      alert('edit');
    } else {
      alert('add');
    }
  }

  update() {
    console.log('update')
  }
}
