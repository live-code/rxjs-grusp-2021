import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, reduce, switchMap, tap, toArray } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
interface Post {
  id: number;
  title: string;
  userId: number;
}
interface User {
  id: number;
  email: string;
  name: string;
}

@Component({
  selector: 'app-demo-switchmap',
  template: `
    <pre>{{ data?.post | json }}</pre>
    <pre>{{ data?.user | json }}</pre>
  `,
})
export class DemoSwitchmapComponent implements OnInit {
  data: { user: User; post: Post };

  constructor(private activate: ActivatedRoute, private http: HttpClient) {
    this.activate.params
      .pipe(
        switchMap(
          ({ id }) => this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${id}`)
        ),
        switchMap(
          post => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
            .pipe(map(user => ({ user, post })))
        ),
      );

    this.doSomething();
  }

  ngOnInit(): void {
  }

  doSomething(): void {
    // EXAMPLE 1: output in HTML page
    this.http
      .get<User[]>('https://jsonplaceholder.typicode.com/users/')
      .pipe(
        switchMap(res => res),
        reduce((acc: number, curr: User) => acc + curr.id, 0)
      )
      .subscribe(res => console.log(res));






  }
}
