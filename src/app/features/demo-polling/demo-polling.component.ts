import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { combineLatest, iif, interval, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { RandomUser } from './random-user';

const API = 'https://randomuser.me/api';
const POLLING_TIME = 3000;


@Component({
  selector: 'app-demo-polling',
  template: `
    {{genderInput.value}}
    <select [formControl]="genderInput">
      <option [value]="''">Select a gender to start polling user</option>
      <option value="female">Female</option>
      <option value="male">Male</option>
    </select>
  `,
})
export class DemoPollingComponent  {
  genderInput: FormControl = new FormControl('');


  constructor(http: HttpClient) {
    /**
     * SOLUTION 1
     */
    /*
    this.genderInput.valueChanges
      .pipe(
        switchMap(
          text => interval(POLLING_TIME)
            .pipe(
              switchMap(() => http.get<RandomUser>(API)),
              map(user => user.results[0])
            )
        )
      )
      .subscribe(console.log)
     */

    /**
     * SOLUTION 2
     */
    /*
    const polling$ = interval(POLLING_TIME);

    const select$ = this.genderInput.valueChanges;

    const getRandom$ = (gender: 'male' | 'female') =>
      http.get<RandomUser>(`${API}?gender=${gender}`)
        .pipe(
          map(user => user.results[0]),
          catchError(() => of(null))
        );

    combineLatest([polling$, select$])
      .pipe(
        map(values => values[1]),
        switchMap(gender => getRandom$(gender)),
      )
      .subscribe(console.log);
    */

    /**
     * SOLUTION 3
     */
    const getRandom$ = (gender: 'male' | 'female') =>
      interval(POLLING_TIME)
        .pipe(
          switchMap(() => http.get<RandomUser>(`${API}?gender=${gender}`)
            .pipe(
              map(user => user.results[0]),
              catchError(() => of(null))
            )
        ));

    this.genderInput.valueChanges
      .pipe(
        switchMap(
          gender => iif(() => gender === '', of(null), getRandom$(gender))
        )
      )
      .subscribe(console.log);

  }

}
