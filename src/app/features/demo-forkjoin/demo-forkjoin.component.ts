import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';

interface User {
  name: string;
}
interface Post {
  title: string;
}

interface Data {
  users: User[];
  posts: Post[];
};

@Component({
  selector: 'app-demo-forkjoin',
  template: `
    {{data?.users.length}} utenti
    {{data?.posts.length}} posts
  `,
})
export class DemoForkjoinComponent implements OnInit {
  USERS = 'https://jsonplaceholder.typicode.com/users/';
  POSTS = 'https://jsonplaceholder.typicode.com/posts/';
  users: User[];
  data: Data;

  constructor(private http: HttpClient) {
    const users$ = this.http.get<User[]>(this.USERS);
    const posts$ = this.http.get<Post[]>(this.POSTS);

    forkJoin({
      users: users$,
      posts: posts$
    }).subscribe(res => {
      console.log(res)
      this.data = res;
    });
  }

  ngOnInit(): void {
  }

}
