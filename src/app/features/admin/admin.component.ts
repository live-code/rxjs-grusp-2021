import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-admin',
  template: `
    <h1>admin</h1>
    <div *ngIf="error" style="color: red">error</div>
    <pre>{{users | json}}</pre>
  `,
})
export class AdminComponent implements OnInit {
  users: any[];
  error: boolean;

  constructor(http: HttpClient) {
    http.get<any[]>('http://localhost:3000/users')
      .subscribe(
        res => this.users = res,
        err => this.error = true
      );
  }

  ngOnInit(): void {
  }

}
