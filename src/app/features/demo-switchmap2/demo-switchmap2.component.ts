import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { concatMap, delay, map, mergeMap, switchMap, take, toArray } from 'rxjs/operators';

const API = 'https://jsonplaceholder.typicode.com';

interface Todo {
  id: number;
  title: string;
  completed: boolean;
  userId: string;
}
interface User {
  id: number;
  email: string;
  name: string;
}

interface UserTodos {
  user: User;
  todos: Todo[];
}

@Component({
  selector: 'app-demo-switchmap2',
  template: `
    <div *ngFor="let u of users">
      <h1>{{ u.user.name}}</h1> 
      <li *ngFor="let todo of u.todos">
        <input type="checkbox" [ngModel]="todo.completed">
        {{todo.title}}
      </li>
    </div>
  `,
  styles: [
  ]
})
export class DemoSwitchmap2Component {
  users: UserTodos[] = [];

  constructor(http: HttpClient) {
    http.get<User[]>(`${API}/users`)
      .pipe(
        switchMap(users => users),
        take(3),
        concatMap(
          user => http.get<Todo[]>(`${API}/todos?userId=${user.id}`)
            .pipe(
              map(todos => ({ todos, user })),
              delay(1000)
            )
        ),
      )
      .subscribe(res => {
        this.users.push(res);
      })
  }


}
