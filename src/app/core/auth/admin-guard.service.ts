import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(): Observable<boolean> {
    return this.authService.role$
      .pipe(
        map(role => role === 'admin'),
        tap(isValid => {
          if (!isValid) {
            this.router.navigateByUrl('login')
          }
        }),
      );
  }
}
