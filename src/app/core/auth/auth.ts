
export type UserRole = 'admin' | 'moderator';

export interface Auth {
  token: string;
  displayName: string;
  role: UserRole;
}
