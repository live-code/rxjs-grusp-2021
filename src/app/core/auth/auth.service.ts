import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth, UserRole } from './auth';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);

  constructor(private http: HttpClient, private router: Router) {
    const auth: Auth = JSON.parse(localStorage.getItem('auth'));
    this.auth$.next(auth);
  }

  login({ username, password }): void {
    const params = new HttpParams()
      .set('username', username)
      .set('password', password);


    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.auth$.next(res);
        this.router.navigateByUrl('admin');
        localStorage.setItem('auth', JSON.stringify(res))
      });
  }

  logout(): void {
    localStorage.removeItem('auth');
    this.auth$.next(null);
    this.router.navigateByUrl('login');
  }

  get displayName$(): Observable<string> {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName)
      );
  }

  get token$(): Observable<string> {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      );
  }

  get role$(): Observable<UserRole> {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      );
  }

  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      );
  }

}
