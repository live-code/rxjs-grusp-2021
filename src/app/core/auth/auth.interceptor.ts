import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { catchError, delay, first, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*let cloned = req;
    if (this.authService.auth$.getValue()) {
      cloned = req.clone({
        setHeaders: {
          Authorization: this.authService.auth$.getValue().token
        }
      });
    }
    return next.handle(cloned);*/
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        switchMap(([isLogged, tk]) => iif(() => isLogged && req.url.includes('localhost'),
            next.handle(req.clone({ setHeaders: { Authorization: tk } })),
            next.handle(req)
        )),
        delay(environment.production ? 0 : 1000),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
                // tooken expired
                break;
              /*
              case 404:
              case 0:
                // this.authService.logout();
                alert('errore grave!')
                break;*/

            }
          }

          return throwError(null); // of
        })
      );
  }

}
