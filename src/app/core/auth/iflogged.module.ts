import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IfloggedDirective } from './iflogged.directive';



@NgModule({
  declarations: [IfloggedDirective],
  exports: [IfloggedDirective],
  imports: [
    CommonModule
  ]
})
export class IfloggedModule { }
