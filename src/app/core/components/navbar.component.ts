import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../theme/theme.service';
import { AuthService } from '../auth/auth.service';
import { Auth } from '../auth/auth';

@Component({
  selector: 'app-navbar',
  template: `
    <div
      [class.dark]="themeService.isDark | async"
      [class.light]="themeService.isLight | async"
    >
      <button routerLink="login">login</button>
      <button routerLink="admin" *ngIf="authService.isLogged$ | async">admin</button>
      <button routerLink="user">user</button>
      <button routerLink="users" *appIfLogged>users</button>
      <button routerLink="settings">settings</button>
      <button
        *ngIf="auth"
        (click)="authService.logout()">
        logout 
        {{(auth?.displayName)}}
      </button>
    </div>
  `,
  styles: [`
    .dark { padding: 10px; background-color: #222; color: white}
    .light { padding: 10px;  background-color: #ccc}
  `]
})
export class NavbarComponent implements OnInit {
  auth: Auth;

  constructor(
    public themeService: ThemeService,
    public authService: AuthService
  ) {
    authService.auth$.subscribe(val => {
      this.auth = val;
    })
  }

  ngOnInit(): void {
  }

}
