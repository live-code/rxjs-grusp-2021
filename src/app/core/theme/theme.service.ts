import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export type Theme = 'dark' | 'light';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  // theme: Theme = 'light';
  theme: BehaviorSubject<Theme> = new BehaviorSubject<Theme>('light');

  setTheme(val: Theme): void {
    this.theme.next(val);
  }

  get isDark(): Observable<boolean> {
    return this.theme
      .pipe(
        map(theme => theme === 'dark')
      );
  }

  get isLight(): Observable<boolean> {
    return this.theme
      .pipe(
        map(theme => theme === 'light')
      );
  }
}
