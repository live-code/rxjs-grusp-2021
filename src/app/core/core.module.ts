import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar.component';
import { RouterModule } from '@angular/router';
import { IfloggedDirective } from './auth/iflogged.directive';
import { IfloggedModule } from './auth/iflogged.module';



@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
  imports: [
    CommonModule,
    RouterModule,
    IfloggedModule
  ]
})
export class CoreModule { }
