import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './features/user/user.component';
import { UsersComponent } from './features/users/users.component';
import { DemoForkjoinComponent } from './features/demo-forkjoin/demo-forkjoin.component';
import { DemoSwitchmapComponent } from './features/demo-switchmap/demo-switchmap.component';
import { DemoSwitchmap2Component } from './features/demo-switchmap2/demo-switchmap2.component';
import { DemoPollingComponent } from './features/demo-polling/demo-polling.component';
import { DemoMeteoExampleComponent } from './features/demo-meteo-example/demo-meteo-example.component';
import { CoreModule } from './core/core.module';
import { AdminGuard } from './core/auth/admin-guard.service';
import { environment } from '../environments/environment';
import { AuthInterceptor } from './core/auth/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UsersComponent,
    DemoForkjoinComponent,
    DemoSwitchmapComponent,
    DemoSwitchmap2Component,
    DemoPollingComponent,
    DemoMeteoExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
